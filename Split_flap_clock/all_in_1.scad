include <face.scad>
include <bottom.scad>
include <StepMotor_28BYJ-48.scad>

$fn=100;
//$fs = 0.15;



union()
{
	render() face();
	translate([63.75+187.3-10,-410.75+2.2,-137.2-2.2]) rotate([-90,0,90]) import("repaired/leftstand.stl", convexity=3);
	translate([-50.75-3,307.8+2.2,-159.4-2.2]) rotate([-90,0,-90]) import("repaired/rightstand.stl", convexity=3);
	translate([187.3,-110,13.2]) rotate([-90,180,0]) bottom();
	// motory
	translate([9.4,-49.2,24.5]) rotate([90,0,90]) StepMotor28BYJ_W();
	translate([100.8,-49.2,24.5]) rotate([-90,0,90]) StepMotor28BYJ_W();	
	translate([177.8,-49.2,24.5]) rotate([-90,0,90]) StepMotor28BYJ_W();
	// rotory
	translate([161.7,3.5,-250.2]) rotate([0,-90,0]) import("repaired/rotor_h.stl", convexity=3);
	translate([-696,169.3,3.5]) import("files/h1.stl", convexity=3);
}





