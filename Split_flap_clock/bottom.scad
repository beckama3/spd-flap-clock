include <threads.scad>

$fn=100;
//$fs = 0.15;

h = 3.5;
x = 187.3;
y = 43;

screw_pos = [[-77,20,h/2], [-77,-20,h/2], [0,0,h/2], [77,-20,h/2], [71,12,h/2]];
spacer_d = 8;
spacer_h = 8;

thread_d = 3;
thread_pitch = 0.5;

//bottom();

module bottom() 
{
	translate([x/2,y/2,h/2]) difference() {
		union() {
			cube([x,y,h], center=true);
			cube([x-20,y+10,h], center=true);
			for (i = [0:len(screw_pos)-1])
			{
				translate(screw_pos[i]) render() difference()
				{
					cylinder(d=spacer_d, h=spacer_h);
					//metric_thread (diameter=thread_d, pitch=thread_pitch, length=spacer_h+1, internal=true);
					translate([0,0,-h/2]) cylinder(d=2.2, h=spacer_h+h);
				}
			}
		}
	}
}



