$fn=100;
//$fs = 0.15;

h = 3.5;
x = 187.3;
y = 84;

x_pseudocenter = 100.85;

//render();
face();

module face() 
{
 //difference(){
	difference() {
		union() {
            

           
			translate([-92.03,253.53,-26.25]) rotate([0,0,0]) import("repaired/face.stl", convexity=3);
			// zakrytie dier
			translate([11,-65,0]) cube([10,50,h]);
    
			translate([166,-65,0]) cube([10,50,h]);
			translate([89,-65,0]) cube([20,50,h]);
			// drziaky motorov
			translate([132.6,-185.4+0.4,-194.95+h]) rotate([-90,0,90]) import("files/mount_left.stl", convexity=3);
			translate([209.6,-185.4+0.4,-194.95+h]) rotate([-90,0,90]) import("files/mount_left.stl", convexity=3);
			translate([-22.35,99.95+0.4,-194.95+h]) rotate([-90,0,-90]) import("files/mount_right.stl", convexity=3);
			translate([21,-17.45,-0.3]) difference()
			{
				l=145;
				d=3.5;
				h=13.77;
				cube([l,d,h]);
				translate([0,0,1.3]) rotate([45,0,0]) cube([2*l+0.02,d,h], center=true);
			}
			// kryt spodnej casti
			translate([10,-y,h]) rotate([180,0,0]) cube([x-20,114.5-y-2.2,h]);
		}
    /////////////////////////
        // diera na kostru displeja
         translate([93.85,-65,1.5]) cube([12,39,2]);
    ///// diera na ramcek displeja   
        translate([93.85,-59,0.5]) cube([12,33,3]);
    ///
     //diera na zobrazovaciu plochu displeja   
       translate([96.85,-59,0]) cube([8,26, 3.5]);   
        
        
        translate([93.85,-65,0.5]) cube([12,4, 3]);
        
        
        //translate([88,-37.5,3.5]) cube([15,14.1, 8]);
    // diery na lepsie zpadanutie motorov   
        translate([14,-95,3.5]) cube([70,42.1, 8]);
        translate([87,-75,3.5]) cube([15,22.1, 8]); 
        translate([160,-75,3.5]) cube([15,22.1, 8]); 
//// logo
       translate([94.5,-111,0.4])scale([0.145, 0.145, 0.1])mirror(v=[1,0,0])
    surface(file = 
    "lev_cvut.png", invert = true);
    translate([53.5,-111,0.4])scale([0.145, 0.145, 0.1])mirror(v=[1,0,0])
    surface(file = 
    "logo_cvut_napis.png", invert = true);
  /// button
  
  translate([95.5,-108,0.4])scale([0.08, 0.08, 0.1]) surface(file = 
"shutdown_button_black.png", invert = true);/// text
 linear_extrude(height=0.4)translate([166,-85,0])mirror(v=[1,0,0])text("KATEDRA",font = "Liberation Sans:style=Bold", size = 7);
 linear_extrude(height=0.4)translate([166,-96,0])mirror(v=[1,0,0])text("MĚŘENÍ",font = "Liberation Sans:style=Bold", size = 7);
 linear_extrude(height=0.4)translate([166,-104,0])mirror(v=[1,0,0])text("meas.fel.cvut.cz",font = "Liberation Sans:style=Bold", size = 5);
            
    //////////////////////////  
        
		// diera na tlacitko
		translate([x_pseudocenter,-114.5+2.2+21.45,0]) cylinder(d=7, h=3*h, center=true); 
	}
    difference ()
    { 
        translate([160,-85,3.5]) cube([12,5, 6]);
        translate([163,-85,3.5]) cube([6,5, 3]);
    }  
  difference ()
    { 
        translate([120,-85,3.5]) cube([12,5, 6]);
        translate([123,-85,3.5]) cube([6,5, 3]);
    }   
  difference ()
    { 
        translate([15,-85,3.5]) cube([12,5, 6]);
        translate([18,-85,3.5]) cube([6,5, 3]);
    }     
    
 /*translate([0,-68,-1]) cube([100,100, 100]);
    translate([96,-120,-1]) cube([96,300, 100]);
    translate([0,-120,-1]) cube([51,300, 100]);
}*/

}




