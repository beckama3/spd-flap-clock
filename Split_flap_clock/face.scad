$fn=100;
//$fs = 0.15;

h = 3.5;
x = 187.3;
y = 84;

x_pseudocenter = 100.85;

//face();

module face() 
{
	difference() {
		union() {
			translate([-92.03,253.53,-26.25]) rotate([0,0,0]) import("repaired/face.stl", convexity=3);
			// zakrytie dier
			translate([11,-65,0]) cube([10,50,h]);
			translate([166,-65,0]) cube([10,50,h]);
			translate([89,-65,0]) cube([20,50,h]);
			// drziaky motorov
			translate([132.6,-185.4+0.4,-194.95+h]) rotate([-90,0,90]) import("files/mount_left.stl", convexity=3);
			translate([209.6,-185.4+0.4,-194.95+h]) rotate([-90,0,90]) import("files/mount_left.stl", convexity=3);
			translate([-22.35,99.95+0.4,-194.95+h]) rotate([-90,0,-90]) import("files/mount_right.stl", convexity=3);
			translate([21,-17.45,-0.3]) difference()
			{
				l=145;
				d=3.5;
				h=13.77;
				cube([l,d,h]);
				translate([0,0,1.3]) rotate([45,0,0]) cube([2*l+0.02,d,h], center=true);
			}
			// kryt spodnej casti
			translate([10,-y,h]) rotate([180,0,0]) cube([x-20,114.5-y-2.2,h]);
		}
		// diera na tlacitko
		translate([x_pseudocenter,-114.5+2.2+21.45,0]) cylinder(d=7, h=3*h, center=true); 
	}
}



