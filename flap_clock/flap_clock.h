#define number_of_74hc595s 2
#define numOfRegisterPins number_of_74hc595s * 8

#define SER_Pin 23   //pin 14 on the 75HC595 ~DS
#define RCLK_Pin 13  //pin 12 on the 75HC595 ~STCP
#define SRCLK_Pin 18 //pin 11 on the 75HC595 ~SHCP

#define BATTERY_PIN 4
#define KILL_PIN 16
#define TURNING_OFF_INTERRUPT_PIN 17

#define CRITICAL_BATTERY_VOLTAGE 6

#define motorSpeed 1    //variable to set stepper speed

#define EEPROM_WIFI_STRINGS_LENGTH 32

//do not change
#define EEPROM_WIFI_AP_ADR 0
#define EEPROM_WIFI_PASS_ADR EEPROM_WIFI_STRINGS_LENGTH
#define EEPROM_ADRESS_HOURS EEPROM_WIFI_PASS_ADR + EEPROM_WIFI_STRINGS_LENGTH
#define EEPROM_ADRESS_10MIN EEPROM_WIFI_PASS_ADR + EEPROM_WIFI_STRINGS_LENGTH + 1
#define EEPROM_ADRESS_01MIN EEPROM_WIFI_PASS_ADR + EEPROM_WIFI_STRINGS_LENGTH + 2

#define EEPROM_SIZE EEPROM_ADRESS_01MIN


//wiring of the step motors
#define pin1_1min 4
#define pin2_1min 5
#define pin3_1min 6
#define pin4_1min 7

#define pin1_10min 8
#define pin2_10min 9
#define pin3_10min 10
#define pin4_10min 11

#define pin1_hour 15 
#define pin2_hour 14 
#define pin3_hour 13 
#define pin4_hour 12

//deadline in seconds until which the clock should attepmt to connect to the wifi to get NTP time
#define WIFI_DEADLINE 30

//define NTP related constants
#define NTP_OFFSET  3600 // In seconds
#define NTP_INTERVAL 60 * 1000    // In miliseconds
#define NTP_ADDRESS  "cz.pool.ntp.org"

#define DAY 86400000 // 86400000 milliseconds in a day
#define HOUR 3600000 // 3600000 milliseconds in an hour
#define MINUTE 60000 // 60000 milliseconds in a minute
#define SECOND 1000 // 1000 milliseconds in a second
