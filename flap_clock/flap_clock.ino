/**
 * @flap_clock.ino
 * @author  Marek Bečka <beckama3@fel.cvut.cz>
 * 
 * @section LICENSE 
 * 
 * Do what the heck do you want. Mention of the original author is appreciated.
 * 
 * 
 * @section DESCRIPTION
 *
 * This file was produced on CTU FEL uni within the course of SPD. Purpose of the code
 * is to control flap clock. It implements hardware drivers of step motors, wifi connection,
 * obtaining time from NTP server and OLED display. 
 * 
 */




#include <WiFi.h>
#include <WebServer.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <EEPROM.h>


#include "flap_clock.h" //all pins, constants etc defined here
#include "flap_clock_hw_functions.h" //all function declared here
#include "oled_cvut.h" //oled display functions here


/*Put your SSID & Password - STA mode, existing wifi which ESP should try to connect to*/
char ssid[EEPROM_WIFI_STRINGS_LENGTH];
char password[EEPROM_WIFI_STRINGS_LENGTH];


/*These are for AP mode, if the connection to the existing wifi fails*/
IPAddress local_ip(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
const char* ssid_AP = "FlapClock";  // Enter SSID here
const char* password_AP = "flapityflap";  //Enter Password here


/*For timing the double dot on display*/
const long eventTime_LDR = 1000; //in ms
unsigned long previous_timer = 0;

boolean registers[numOfRegisterPins];

float voltage;

int ntp_hours;
int ntp_minutes;
int ntp_seconds;

int timer_hours;
int timer_minutes;
int timer_seconds;

int hours;
int minutes;
int seconds;

int req_flap_hours;
int req_flap_10_mins;
int req_flap_01_mins;

volatile int flap_hours;
volatile int flap_10_mins;
volatile int flap_01_mins;

unsigned long ntp_start_timer;
unsigned long current_timer;

boolean displaying_time = false;
boolean colon_state = false;
boolean display_colon = false;

WebServer server(80);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);




void setup() {
  Serial.begin(9600);
  EEPROM.begin(EEPROM_SIZE);

  //setup correct modes of pins
  setup_pinmodes();

  //checks the state of flaps
  read_eeprom_wifi();

  //reset all register pins
  clearRegisters();
  writeRegisters();

  setup_oled();
  check_critical_voltage(false);

  Serial.println("Connecting to ");
  Serial.println(ssid);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //ESP tries to connect to wifi within WIFI_DEADLINE [sec]
  int deadline = WIFI_DEADLINE;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
    deadline--;
    if (deadline == 0) {
      Serial.println("WiFi not connected..!");
      break;
    }
  }

  //this branch executes if the clock are connected to wifi
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("");
    Serial.println("WiFi connected..!");
    Serial.print("Got IP: ");  Serial.println(WiFi.localIP());
    setup_NTP();
    delay(100);
    tuning_done_EEPROM();

    //this branch executes if the clock fail to connect to wifi
  } else {
    WiFi.disconnect();
    delay(100);
    Serial.println("Starting AP mode!");
    WiFi.softAP(ssid_AP, password_AP);
    WiFi.softAPConfig(local_ip, gateway, subnet);
    delay(100);
    setup_HTTP_server();
  }
}


void loop() {

  /* Timer for second-flashing double dot*/
  current_timer = millis();

  /* Check if the clock is about to turn off */
  check_interrupt();

  /* This is executed every second */
  if ( current_timer - previous_timer >= eventTime_LDR) {

    get_time();

    /* oled colon */
    oled_display();

    /*The clock has been calibrated, lets start displaying the time*/
    if (displaying_time) {
      convert_ami_time();
      while (flap_01_mins != req_flap_01_mins) {
        plus_1min(false);
      }
      while (flap_10_mins != req_flap_10_mins) {
        plus_10min(false);
      }
      while (flap_hours != req_flap_hours) {
        plus_hour(false);
      }
      display_colon = true;
      //WiFi.mode(WIFI_OFF); //need to experiment with this

    }

    previous_timer = current_timer;
  }
  server.handleClient();
}


/**
   Function that reads from EEPROM. The read values of ssid and password are stored
   in coresponding local variables.
*/
void read_eeprom_wifi() {
  for (int i = 0; i < EEPROM_WIFI_STRINGS_LENGTH; i++)
  {
    ssid[i] = char(EEPROM.read(i + EEPROM_WIFI_AP_ADR));
  }
  Serial.println();
  Serial.print("SSID eeprom: ");
  Serial.println(ssid);
  Serial.println("Reading EEPROM pass");

  for (int i = 0; i < EEPROM_WIFI_STRINGS_LENGTH; i++)
  {
    password[i] = char(EEPROM.read(i + EEPROM_WIFI_PASS_ADR));
  }
  Serial.println();
  Serial.print("password eeprom: ");
  Serial.println(password);
  Serial.println("Reading EEPROM pass");
}

/**
   Function called in setup, for the pin modes.
*/
void setup_pinmodes() {
  pinMode(SER_Pin, OUTPUT);
  pinMode(RCLK_Pin, OUTPUT);
  pinMode(SRCLK_Pin, OUTPUT);
  pinMode(15, INPUT);  //aby fungovalo vypinani, mozna detekce nabijecky

  digitalWrite(KILL_PIN, HIGH); //first write high state
  pinMode(KILL_PIN, OUTPUT); //then specify as output -> ESP stays on

  pinMode(BATTERY_PIN, INPUT);
  pinMode(TURNING_OFF_INTERRUPT_PIN, INPUT);
}

/**
   Reads voltage of a battery, the value in volts is stored in voltage variable, type float.
*/
void battery_voltage() {
  voltage = analogRead(BATTERY_PIN);
  voltage = voltage / 276;
  Serial.print("Napeti: ");
  Serial.print(voltage);
  Serial.print("\n");
}

/**
   Calls a battery_voltage() function. If the battery is discharged below critical level
   specified in flap_clock.h, on the OLED drained battery flashes and the clock is turned
   off.
*/
void check_critical_voltage(boolean display_battery_level) {
  battery_voltage();
  if (voltage < CRITICAL_BATTERY_VOLTAGE) {
    write_state_EEPROM();
    Serial.println("Kriticke napeti baterie, vypinam!!!");
    for (int x; x < 3; x++) {
      clear_oled();
      delay(1000);
      display_battery_exclamation();
      delay(1000);
    }

    digitalWrite(KILL_PIN, LOW); //this shuts down the clock's battery power

    while (1) { //the clock stays in this loop if the clock are in developer mode (ESP is connedted to PC - power supply is from USB)
      clear_oled();
      delay(1000);
      Serial.println("Kriticke napeti baterie, hodiny maji vypnute napajeni z baterie.");
      display_battery_exclamation();
      delay(1000);
    }
  }

  //probably implement this feature better way
  if (display_battery_level) {
    Serial.println("TO DO: display % of battery on OLED");
  }
}


/**
   Writes states of all flaps to EEPROM.
*/
void write_state_EEPROM() {
  EEPROM.write(EEPROM_ADRESS_HOURS, flap_hours);
  EEPROM.write(EEPROM_ADRESS_10MIN, flap_10_mins);
  EEPROM.write(EEPROM_ADRESS_01MIN, flap_01_mins);

  Serial.print("EEPROM written: ");
  Serial.print(flap_hours);
  Serial.print(":");
  Serial.print(flap_10_mins);
  Serial.println(flap_01_mins);

  EEPROM.commit();
}

/**
   Handler of http server.
   Reads ssid and password from http server arguments and stores them to EEPROM.
*/
void handle_wifi_form() {
  if (server.args() > 0 ) {
    for (int i = 0; i < server.args(); i++ ) {
      int x;
      if (server.argName(i) == "Password") {
        // do something here with value from server.arg(i);
        String qpass = server.arg(i);
        for (int x = 0; x < EEPROM_WIFI_STRINGS_LENGTH; x++) {
          EEPROM.write(x + EEPROM_WIFI_PASS_ADR, qpass[x]);

          Serial.print("Adress: ");
          Serial.print(x + EEPROM_WIFI_PASS_ADR);
          Serial.print(" Wrote: ");
          Serial.println(qpass[x]);
        }
        EEPROM.commit();
      }

      if (server.argName(i) == "AP") {
        // do something here with value from server.arg(i);
        String qssid = server.arg(i);
        for (int x = 0; x < EEPROM_WIFI_STRINGS_LENGTH; x++) {
          EEPROM.write(x + EEPROM_WIFI_AP_ADR, qssid[x]);
        }
        EEPROM.commit();
      }
    }
  }
  server.send(200, "text/html", SendHTML());
}

/**
   Setup of http server.
   Assigns handlers to all sitelinks (eg. 192.168.1.1/tune01mins -> tune01mins function is called)
*/
void setup_HTTP_server() {
  server.on("/", handle_OnConnect);
  server.on("/tune01mins", tune01mins);
  server.on("/tune10mins", tune10mins);
  server.on("/tunehour", tunehour);
  server.on("/finetune01mins", finetune01mins);
  server.on("/finetune10mins", finetune10mins);
  server.on("/finetunehour", finetunehour);
  server.on("/tuning_done", tuning_done);
  server.on("/submitWifi", handle_wifi_form);

  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");
}


/**
   Starts a timeClient and tries to get a time. The moment the time is obtained, relative
   time of the ESP from startup is noted to ntp_start_timer variable.
*/
void setup_NTP() {
  timeClient.begin();
  Serial.println("NTP Client started");

  if (!timeClient.update()) {
    Serial.println("Failed to get NTP time!");
    WiFi.mode( WIFI_MODE_NULL );
  }

  ntp_hours = timeClient.getHours();
  ntp_minutes = timeClient.getMinutes();
  ntp_seconds = timeClient.getSeconds();
  ntp_start_timer = millis();
}


/**
   This computes current time from (current_timer - ntp_start_timer) + ntp time.
*/
void get_time() {
  unsigned milis_from_start = current_timer - ntp_start_timer;
  unsigned milis_from_midnight = milis_from_start + (SECOND * ntp_seconds) + (MINUTE * ntp_minutes) + (HOUR * ntp_hours);
  milis_from_midnight = milis_from_midnight % DAY; //get rid of days

  timer_hours = milis_from_midnight / HOUR;
  timer_minutes = (milis_from_midnight % HOUR) / MINUTE;


  //  Serial.println("~~~~ START ~~~~");
  //  Serial.print("timer_hours: ");
  //  Serial.print(timer_hours);
  //  Serial.print(" , timer_minutes: ");
  //  Serial.print(timer_minutes);
  //  Serial.print("\n\n");

  hours = timer_hours;
  minutes = timer_minutes;

  //  Serial.println("Start:");
  //  Serial.print("ntp_hours: ");
  //  Serial.print(flap_01_mins);
  //  Serial.print(" vs hours: ");
  //  Serial.print(hours);
  //  Serial.print("\n");
  //
  //  Serial.print("ntp_minutes: ");
  //  Serial.print(ntp_minutes);
  //  Serial.print(" vs minutes: ");
  //  Serial.print(minutes);
  //  Serial.print("\n\n");


  minutes = minutes % 60;
  hours = hours % 24;

  //  Serial.print("ntp_hours: ");
  //  Serial.print(flap_01_mins);
  //  Serial.print(" vs hours: ");
  //  Serial.print(hours);
  //  Serial.print("\n");

  //  Serial.print("ntp_minutes: ");
  //  Serial.print(ntp_minutes);
  //  Serial.print(" vs minutes: ");
  //  Serial.print(minutes);
  //  Serial.print("\n\n");
}

/**
   Checks if the off button has been pressed. If so, the clock is about to turn off, so this
   function write the state of all flaps to EEPROM.
*/
void check_interrupt() {
  boolean turning_off_interrupt;
  turning_off_interrupt = digitalRead(17);
  if (turning_off_interrupt == LOW) {
    write_state_EEPROM();
    Serial.println("Turning off, state written to eeprom!");
  }
}


/**
   Converts the time of standard format (eg. 13:58) to the stupid american format (eg. 1:58)
*/
void convert_ami_time() {

  if (hours == 0) {
    req_flap_hours = 12; //makes sense #amiftw
  } else if (hours > 12) {
    req_flap_hours = hours - 12;
  } else {
    req_flap_hours = hours;
  }

  req_flap_10_mins = minutes / 10;
  req_flap_01_mins = minutes - (minutes / 10) * 10;

}

/**
   Handler of http server.
*/
void handle_OnConnect() {
  server.send(200, "text/html", SendHTML());
  Serial.println("handle_OnConnect metoda");
}


/**
   Handler of http server.
   Plus one 01 mins flap.
*/
void tune01mins() {
  plus_1min(true);
  server.send(200, "text/html", SendHTML());
}

/**
   Handler of http server.
   Plus one 10 mins flap.
*/


void tune10mins() {
  plus_10min(true);
  server.send(200, "text/html", SendHTML());
}

/**
   Handler of http server.
   Plus one hour flap.
*/
void tunehour() {
  plus_hour(true);
  server.send(200, "text/html", SendHTML());
}

/**
   Handler of http server.
   Adds 10 steps to the 01 mins flap.
*/
void finetune01mins() {
  for (int x = 0; x < 16; x++) {
    step_1min();
  }
  server.send(200, "text/html", SendHTML());

  //reset all register pins
  clearRegisters();
  writeRegisters();
}


/**
   Handler of http server.
   Adds 10 steps to the 10 mins flap.
*/
void finetune10mins() {
  for (int x = 0; x < 16; x++) {
    step_10min();
  }
  server.send(200, "text/html", SendHTML());

  //reset all register pins
  clearRegisters();
  writeRegisters();
}

/**
   Handler of http server.
   Adds 10 steps to the hour flap.
*/
void finetunehour() {
  for (int x = 0; x < 16; x++) {
    step_hour();
  }
  server.send(200, "text/html", SendHTML());

  //reset all register pins
  clearRegisters();
  writeRegisters();
}


/**
   Handler of http server.
   Called when the DONE button is pressed. That should be after clock calubration (1:00 displayed).
*/
void tuning_done() {
  flap_hours = 1;
  flap_10_mins = 0;
  flap_01_mins = 0;

  //server.send(200, "text/html", SendHTML());
  server.send(200, "text/plain", "Restarting");

  write_state_EEPROM();

  delay(1000);

  //restarting the ESP, so it can try to reconnect to the new wifi network.
  ESP.restart();
}


/**
   This is called if the clock succesfully obtain NTP time. It reads the state of the flaps from
   EEPROM and saves it to global variables.
*/
void tuning_done_EEPROM() {
  flap_hours = EEPROM.read(EEPROM_ADRESS_HOURS);
  flap_10_mins = EEPROM.read(EEPROM_ADRESS_10MIN);
  flap_01_mins = EEPROM.read(EEPROM_ADRESS_01MIN);

  Serial.print("EEPROM: ");
  Serial.print(flap_hours);
  Serial.print(", ");
  Serial.print(flap_10_mins);
  Serial.print(", ");
  Serial.print(flap_01_mins);
  Serial.print("\n");

  delay(100);

  displaying_time = true; //clock start to display the correct time
}

/**
   Handler of http server.
*/
void handle_NotFound() {
  server.send(404, "text/plain", "Not found");
}


/**
   Main HTML code.
*/
String SendHTML() {
  String ptr = "<!DOCTYPE html> <html>\n";

  //magic begins here
  ptr += "<head><meta name=\"viewport\" content=\"width=device-height, initial-scale=0.45, user-scalable=no\">\n";
  ptr += "<title>Flap clock</title>\n";
  ptr += "<style>html { font-family: Helvetica; font-size: 2em; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr += "body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr += ".button {display: block;width: 200px;background-color: #3498db;border: none;color: white;padding: 20px 30px;text-decoration: none;font-size: 27px;margin: 0px 20px 15px;cursor: pointer;border-radius: 4px; float: left;}\n";
  ptr += ".button-on {background-color: #3498db;}\n";
  ptr += ".button-on:active {background-color: #2980b9;}\n";
  ptr += ".button-off {background-color: #34495e;}\n";
  ptr += ".button-off:active {background-color: #2c3e50;}\n";
  ptr += "p {font-size: 19px;color: #888;margin-bottom: 10px;}\n";
  ptr += ".row {width: 100%;height: 100px;}\n";
  ptr += ".middle {width: 600px;height: 100px;margin: auto}\n";
  ptr += ".done {float: none;margin: 30px auto;}\n";
  ptr += "</style>\n";
  ptr += "</head>\n";


  ptr += "<body>\n";
  ptr += "<h1>Flap clock</h1>\n";

  //button tune01mins, finetune01mins
  ptr += "<div class=\"row\"><p>Tune 01 min flap to 0!</p><div class=\"middle\"><a class=\"button button-on\" href=\"/tune01mins\">ADD 1 MIN</a>\n";
  ptr += "<a class=\"button button-on\" href=\"/finetune01mins\">FINE TUNE</a></div></div>\n";

  //button tune10mins, finetune10mins
  ptr += "<div class=\"row\"><p>Tune 10 min flap 0!</p><div class=\"middle\"><a class=\"button button-on\" href=\"/tune10mins\">ADD 10 MINS</a>\n";
  ptr += "<a class=\"button button-on\" href=\"/finetune10mins\">FINE TUNE</a></div></div>\n";

  //button tunehour, finetunehour
  ptr += "<div class=\"row\"><p>Tune hour to 1!</p><div class=\"middle\"><a class=\"button button-on\" href=\"/tunehour\">ADD HOUR</a>\n";
  ptr += "<a class=\"button button-on\" href=\"/finetunehour\">FINE TUNE</a></div></div>\n";

  //button done
  ptr += "<div class=\"row\"><a class=\"button button-off done\" href=\"/tuning_done\">DONE</a></div>\n";


  ptr += "<div class=\"row\"><p>Need to tune the wifi network? Do it here:</p></div>\n";

  //the true magic (aka the form for wifi)
  ptr += "<form action='http://192.168.1.1/submitWifi' method='POST'>";
  ptr += "AP: <input type=\"text\" name=\"AP\" value=\"";
  ptr += ssid;
  ptr += "\"><br>";
  ptr += "Password: <input type=\"text\" name=\"Password\" value=\"";
  ptr += password;
  ptr += "\"><br>";
  ptr += "<input type=\"submit\" value=\"Submit\">";
  ptr += "</form>";


  ptr += "</body>\n";
  ptr += "</html>\n";

  return ptr;
}
