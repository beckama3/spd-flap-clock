#include "flap_clock_hw_functions.h"
#include "flap_clock.h"



//for determining which step is in order
int switch_1min = 0;
int switch_10min = 0;
int switch_hour = 0;

boolean boolean_plus_1min = 0;


/*
   Sets all register pins to LOW
*/
void clearRegisters() {
  for (int i = numOfRegisterPins - 1; i >=  0; i--) {
    registers[i] = LOW;
  }
}

/*
   Writes values from registers[i] to the registers
*/
void writeRegisters() {

  digitalWrite(RCLK_Pin, LOW);

  for (int i = numOfRegisterPins - 1; i >=  0; i--) {

    digitalWrite(SRCLK_Pin, LOW);
    int val = registers[i];
    digitalWrite(SER_Pin, val);
    digitalWrite(SRCLK_Pin, HIGH);
  }
  digitalWrite(RCLK_Pin, HIGH);

}


/*
   Sets an individual pin HIGH or LOW
*/
void setRegisterPin(int index, int value) {
  registers[index] = value;
}



/*
 * 
 * 
 * 
 * plus_xflap methods
 * 
 * 
 * 
 */
void plus_1min(boolean tuning) {
  for (int x = 0; x < 409; x++) {
    step_1min();
  }

  if (boolean_plus_1min) {
    step_1min();
  }


  boolean_plus_1min = !boolean_plus_1min;

  if (!tuning) {
    flap_01_mins++;
    if (flap_01_mins == 10) {
      flap_01_mins = 0;
      step_1min();
    }
  }
  clearRegisters();
  writeRegisters();
}


void plus_10min(boolean tuning) {
  switch (flap_10_mins) {
    case 2:
      step_10min();
      break;
    case 4:
      step_10min();
      break;
  }

  //Serial.println("deafault plus_10min");
  for (int x = 0; x < 341; x++) {
    step_10min();
  }

  if (!tuning) {
    flap_10_mins++;
    if (flap_10_mins == 6) {
      flap_10_mins = 0;
    }
  }

  clearRegisters();
  writeRegisters();
}



void plus_hour(boolean tuning) {

  switch (flap_hours) {

    case 2:
      step_hour();
      break;
    case 4:
      step_hour();
      break;
    case 8:
      step_hour();
      break;
    case 10:
      step_hour();
      break;
  }

  //Serial.println("deafault plus_10min");
  for (int x = 0; x < 341; x++) {
    step_hour();
  }

  if (!tuning) {
    flap_hours++;
    if (flap_hours == 13) {
      flap_hours = 0;
    }
  }
  clearRegisters();
  writeRegisters();
}




/*
 * 
 * 
 * 
 * 
 * STEPS
 * 
 * 
 * 
 * 
 */

void step_1min() {
  switch (switch_1min) {
    case 0:
      setRegisterPin(pin1_1min, LOW);
      setRegisterPin(pin2_1min, LOW);
      setRegisterPin(pin3_1min, LOW);
      setRegisterPin(pin4_1min, HIGH);
      break;

    case 1:
      setRegisterPin(pin1_1min, LOW);
      setRegisterPin(pin2_1min, LOW);
      setRegisterPin(pin3_1min, HIGH);
      setRegisterPin(pin4_1min, HIGH);
      break;

    case 2:
      setRegisterPin(pin1_1min, LOW);
      setRegisterPin(pin2_1min, LOW);
      setRegisterPin(pin3_1min, HIGH);
      setRegisterPin(pin4_1min, LOW);
      break;

    case 3:
      setRegisterPin(pin1_1min, LOW);
      setRegisterPin(pin2_1min, HIGH);
      setRegisterPin(pin3_1min, HIGH);
      setRegisterPin(pin4_1min, LOW);
      break;

    case 4:
      setRegisterPin(pin1_1min, LOW);
      setRegisterPin(pin2_1min, HIGH);
      setRegisterPin(pin3_1min, LOW);
      setRegisterPin(pin4_1min, LOW);
      break;

    case 5:
      setRegisterPin(pin1_1min, HIGH);
      setRegisterPin(pin2_1min, HIGH);
      setRegisterPin(pin3_1min, LOW);
      setRegisterPin(pin4_1min, LOW);
      break;

    case 6:
      setRegisterPin(pin1_1min, HIGH);
      setRegisterPin(pin2_1min, LOW);
      setRegisterPin(pin3_1min, LOW);
      setRegisterPin(pin4_1min, LOW);
      break;

    case 7:
      setRegisterPin(pin1_1min, HIGH);
      setRegisterPin(pin2_1min, LOW);
      setRegisterPin(pin3_1min, LOW);
      setRegisterPin(pin4_1min, HIGH);
      break;

    default:
      setRegisterPin(pin1_1min, LOW);
      setRegisterPin(pin2_1min, LOW);
      setRegisterPin(pin3_1min, LOW);
      setRegisterPin(pin4_1min, LOW);
      break;
  }
  writeRegisters();
  switch_1min++;
  if (switch_1min > 7) {
    switch_1min = 0;
  }
  delay(motorSpeed);
}


void step_10min() {
  switch (switch_10min) {
    case 0:
      setRegisterPin(pin1_10min, LOW);
      setRegisterPin(pin2_10min, LOW);
      setRegisterPin(pin3_10min, LOW);
      setRegisterPin(pin4_10min, HIGH);
      break;

    case 1:
      setRegisterPin(pin1_10min, LOW);
      setRegisterPin(pin2_10min, LOW);
      setRegisterPin(pin3_10min, HIGH);
      setRegisterPin(pin4_10min, HIGH);
      break;

    case 2:
      setRegisterPin(pin1_10min, LOW);
      setRegisterPin(pin2_10min, LOW);
      setRegisterPin(pin3_10min, HIGH);
      setRegisterPin(pin4_10min, LOW);
      break;

    case 3:
      setRegisterPin(pin1_10min, LOW);
      setRegisterPin(pin2_10min, HIGH);
      setRegisterPin(pin3_10min, HIGH);
      setRegisterPin(pin4_10min, LOW);
      break;

    case 4:
      setRegisterPin(pin1_10min, LOW);
      setRegisterPin(pin2_10min, HIGH);
      setRegisterPin(pin3_10min, LOW);
      setRegisterPin(pin4_10min, LOW);
      break;

    case 5:
      setRegisterPin(pin1_10min, HIGH);
      setRegisterPin(pin2_10min, HIGH);
      setRegisterPin(pin3_10min, LOW);
      setRegisterPin(pin4_10min, LOW);
      break;

    case 6:
      setRegisterPin(pin1_10min, HIGH);
      setRegisterPin(pin2_10min, LOW);
      setRegisterPin(pin3_10min, LOW);
      setRegisterPin(pin4_10min, LOW);
      break;

    case 7:
      setRegisterPin(pin1_10min, HIGH);
      setRegisterPin(pin2_10min, LOW);
      setRegisterPin(pin3_10min, LOW);
      setRegisterPin(pin4_10min, HIGH);
      break;

    default:
      setRegisterPin(pin1_10min, LOW);
      setRegisterPin(pin2_10min, LOW);
      setRegisterPin(pin3_10min, LOW);
      setRegisterPin(pin4_10min, LOW);
      break;
  }
  writeRegisters();

  switch_10min++;
  if (switch_10min > 7) {
    switch_10min = 0;
  }
  delay(motorSpeed);
}


void step_hour() {
  switch (switch_hour) {
    case 0:
      setRegisterPin(pin1_hour, LOW);
      setRegisterPin(pin2_hour, LOW);
      setRegisterPin(pin3_hour, LOW);
      setRegisterPin(pin4_hour, HIGH);
      break;
    case 1:
      setRegisterPin(pin1_hour, LOW);
      setRegisterPin(pin2_hour, LOW);
      setRegisterPin(pin3_hour, HIGH);
      setRegisterPin(pin4_hour, HIGH);
      break;
    case 2:
      setRegisterPin(pin1_hour, LOW);
      setRegisterPin(pin2_hour, LOW);
      setRegisterPin(pin3_hour, HIGH);
      setRegisterPin(pin4_hour, LOW);
      break;
    case 3:
      setRegisterPin(pin1_hour, LOW);
      setRegisterPin(pin2_hour, HIGH);
      setRegisterPin(pin3_hour, HIGH);
      setRegisterPin(pin4_hour, LOW);
      break;
    case 4:
      setRegisterPin(pin1_hour, LOW);
      setRegisterPin(pin2_hour, HIGH);
      setRegisterPin(pin3_hour, LOW);
      setRegisterPin(pin4_hour, LOW);
      break;
    case 5:
      setRegisterPin(pin1_hour, HIGH);
      setRegisterPin(pin2_hour, HIGH);
      setRegisterPin(pin3_hour, LOW);
      setRegisterPin(pin4_hour, LOW);
      break;
    case 6:
      setRegisterPin(pin1_hour, HIGH);
      setRegisterPin(pin2_hour, LOW);
      setRegisterPin(pin3_hour, LOW);
      setRegisterPin(pin4_hour, LOW);
      break;
    case 7:
      setRegisterPin(pin1_hour, HIGH);
      setRegisterPin(pin2_hour, LOW);
      setRegisterPin(pin3_hour, LOW);
      setRegisterPin(pin4_hour, HIGH);
      break;
    default:
      setRegisterPin(pin1_hour, LOW);
      setRegisterPin(pin2_hour, LOW);
      setRegisterPin(pin3_hour, LOW);
      setRegisterPin(pin4_hour, LOW);
      break;
  }
  writeRegisters();

  switch_hour++;
  if (switch_hour > 7) {
    switch_hour = 0;
  }
  delay(motorSpeed);
}
